package com.cryptocurrencies.di.module

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.cryptocurrencies.ui.CryptocurrencyViewModelFactory
import com.cryptocurrencies.utils.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by GrinfeldRa
 */
@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideUtils(): Utils = Utils(app)

    @Provides
    @Singleton
    fun provideCryptocurrenciesViewModelFactory(
        factory: CryptocurrencyViewModelFactory
    ): ViewModelProvider.Factory = factory

}

