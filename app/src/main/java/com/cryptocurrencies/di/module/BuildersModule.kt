package com.cryptocurrencies.di.module

import com.cryptocurrencies.ui.CryptocurrencyActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by GrinfeldRa
 */
@Module
abstract class BuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeCryptocurrenciesActivity(): CryptocurrencyActivity
}