package com.cryptocurrencies.di.component

import com.cryptocurrencies.App
import com.cryptocurrencies.di.module.AppModule
import com.cryptocurrencies.di.module.BuildersModule
import com.cryptocurrencies.di.module.NetModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by GrinfeldRa
 */
@Singleton
@Component(
    modules = [(AndroidInjectionModule::class), (BuildersModule::class), (AppModule::class), (NetModule::class)]
)
interface AppComponent {
    fun inject(app: App)
}