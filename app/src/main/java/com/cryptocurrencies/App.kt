package com.cryptocurrencies

import android.app.Activity
import android.app.Application
import com.cryptocurrencies.di.component.DaggerAppComponent
import com.cryptocurrencies.di.module.AppModule
import com.cryptocurrencies.di.module.NetModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by GrinfeldRa
 */
class App : Application(), HasActivityInjector{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule("https://pro-api.coinmarketcap.com/v1/"))
            .build()
            .inject(this)
        Timber.plant(Timber.DebugTree())
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

}