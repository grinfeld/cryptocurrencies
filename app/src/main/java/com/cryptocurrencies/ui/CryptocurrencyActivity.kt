package com.cryptocurrencies.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.cryptocurrencies.R
import com.cryptocurrencies.data.model.Cryptoсurrency
import com.cryptocurrencies.utils.InfiniteScrollListener
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_cryptocurrencies.*
import java.util.ArrayList
import javax.inject.Inject


/**
 * Created by GrinfeldRa
 */
class CryptocurrencyActivity : AppCompatActivity() {


    @Inject
    lateinit var cryptoCurrencyViewModelFactory: CryptocurrencyViewModelFactory
    private var cryptocurrenciesAdapter = CryptocurrencyAdapter(ArrayList())
    private lateinit var cryptoCurrencyViewModel: CryptocurrencyViewModel
    private var currentPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cryptocurrencies)
        AndroidInjection.inject(this)

        initializeRecycler()

        cryptoCurrencyViewModel = ViewModelProviders.of(this, cryptoCurrencyViewModelFactory).get(
            CryptocurrencyViewModel::class.java)

        progressBar.visibility = View.VISIBLE
        loadData()

        cryptoCurrencyViewModel.cryptocurrenciesResult().observe(this,
            Observer<List<Cryptoсurrency>> {
                if (it != null) {
                    val position = cryptocurrenciesAdapter.itemCount
                    cryptocurrenciesAdapter.addCryptocurrencies(it)
                    recycler.adapter = cryptocurrenciesAdapter
                    recycler.scrollToPosition(position - 3)
                }
            })

        cryptoCurrencyViewModel.cryptocurrenciesError().observe(this, Observer<String> {
            if (it != null) {
                //toast(resources.getString(R.string.cryptocurrency_error_message) + it)

            }
        })

        cryptoCurrencyViewModel.cryptocurrenciesLoader().observe(this, Observer<Boolean> {
            if (it == false) progressBar.visibility = View.GONE
        })
    }


    private fun initializeRecycler() {
        val gridLayoutManager = GridLayoutManager(this, 1)
        recycler.apply {
            setHasFixedSize(true)
            layoutManager = gridLayoutManager
            addOnScrollListener(InfiniteScrollListener({ loadData() }, gridLayoutManager))
        }
    }


    private fun loadData() {
        cryptoCurrencyViewModel.loadCryptocurrencies(1,10
        )
        currentPage++
    }

    override fun onDestroy() {
        cryptoCurrencyViewModel.disposeElements()
        super.onDestroy()
    }

}