package com.cryptocurrencies.ui


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import javax.inject.Inject


/**
 * Created by GrinfeldRa
 */
class CryptocurrencyViewModelFactory @Inject constructor(
    private val cryptoCurrencyViewModel: CryptocurrencyViewModel) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CryptocurrencyViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return cryptoCurrencyViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}