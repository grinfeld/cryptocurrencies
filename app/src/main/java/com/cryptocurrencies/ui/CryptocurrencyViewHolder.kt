package com.cryptocurrencies.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cryptocurrencies.R
import com.cryptocurrencies.data.model.Cryptoсurrency
import kotlinx.android.synthetic.main.cryptocurrency_list_item.view.*

/**
 * Created by GrinfeldRa
 */
class CryptocurrencyViewHolder(parent : ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.cryptocurrency_list_item, parent, false)) {


    fun bindTo(cryptoCurrencyItem: Cryptoсurrency?) {
        itemView.cryptocurrency_id.text = cryptoCurrencyItem?.name
        itemView.cryptocurrency_rate.text = cryptoCurrencyItem?.lastUpdated
    }
}