package com.cryptocurrencies.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cryptocurrencies.data.model.Cryptoсurrency
import com.cryptocurrencies.data.repository.CryptoсurrencyRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by GrinfeldRa
 */
class CryptocurrencyViewModel @Inject constructor(
    private val cryptoCurrencyRepository: CryptoсurrencyRepository
) : ViewModel() {

    var cryptocurrenciesResult: MutableLiveData<List<Cryptoсurrency>> = MutableLiveData()
    var cryptocurrenciesError: MutableLiveData<String> = MutableLiveData()
    var cryptocurrenciesLoader: MutableLiveData<Boolean> = MutableLiveData()
    private lateinit var disposableObserver: DisposableObserver<List<Cryptoсurrency>>

    fun cryptocurrenciesResult(): LiveData<List<Cryptoсurrency>> {
        return cryptocurrenciesResult
    }

    fun cryptocurrenciesError(): LiveData<String> {
        return cryptocurrenciesError
    }

    fun cryptocurrenciesLoader(): LiveData<Boolean> {
        return cryptocurrenciesLoader
    }

    fun loadCryptocurrencies(limit: Int, offset: Int) {

        disposableObserver = object : DisposableObserver<List<Cryptoсurrency>>() {
            override fun onComplete() {

            }

            override fun onNext(cryptoCurrencies: List<Cryptoсurrency>) {
                Timber.log( 1,cryptoCurrencies.toString())
                cryptocurrenciesResult.postValue(cryptoCurrencies)
                cryptocurrenciesLoader.postValue(false)
            }

            override fun onError(e: Throwable) {
                cryptocurrenciesError.postValue(e.message)
                cryptocurrenciesLoader.postValue(false)
            }
        }

        cryptoCurrencyRepository.getCryptocurrencies()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(disposableObserver)
    }

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
    }



}