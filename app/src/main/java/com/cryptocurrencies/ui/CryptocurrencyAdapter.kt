package com.cryptocurrencies.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cryptocurrencies.R
import com.cryptocurrencies.data.model.Cryptoсurrency

/**
 * Created by GrinfeldRa
 */
class CryptocurrencyAdapter(
    cryptoCurrencies: List<Cryptoсurrency>?) : RecyclerView.Adapter<CryptocurrencyAdapter.CryptocurrencyViewHolder>() {

    private var cryptocurrenciesList = ArrayList<Cryptoсurrency>()

    init {
        this.cryptocurrenciesList = cryptoCurrencies as ArrayList<Cryptoсurrency>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CryptocurrencyViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(
            R.layout.cryptocurrency_list_item,
            parent, false)
        return CryptocurrencyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return cryptocurrenciesList.size
    }

    override fun onBindViewHolder(holder: CryptocurrencyViewHolder, position: Int) {
        val cryptocurrencyItem = cryptocurrenciesList[position]
        holder?.cryptocurrencyListItem(cryptocurrencyItem)
    }

    fun addCryptocurrencies(cryptoCurrencies: List<Cryptoсurrency>) {
        val initPosition = cryptocurrenciesList.size
        cryptocurrenciesList.addAll(cryptoCurrencies)
        notifyItemRangeInserted(initPosition, cryptocurrenciesList.size)
    }

    class CryptocurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var cryptocurrencyId = itemView.findViewById<TextView>(R.id.cryptocurrency_id)!!
        private var cryptocurrencyRate = itemView.findViewById<TextView>(R.id.cryptocurrency_rate)!!

        fun cryptocurrencyListItem(cryptoCurrencyItem: Cryptoсurrency) {
            cryptocurrencyId.text = cryptoCurrencyItem.name
            cryptocurrencyRate.text = cryptoCurrencyItem.lastUpdated
        }
    }
}