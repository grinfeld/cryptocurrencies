package com.cryptocurrencies.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by GrinfeldRa
 */
class Data<T> {

    @SerializedName("data")
    var response: T? = null
}
