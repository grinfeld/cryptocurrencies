package com.cryptocurrencies.data.repository

import com.cryptocurrencies.data.model.Cryptoсurrency
import com.cryptocurrencies.data.remote.ApiInterface
import com.cryptocurrencies.utils.Utils
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by GrinfeldRa
 */
class CryptoсurrencyRepository @Inject constructor(private val apiInterface: ApiInterface,
                                                   private val utils: Utils
) {

    fun getCryptocurrencies(): Observable<List<Cryptoсurrency>> {
        val hasConnection = utils.isConnectedToInternet()
        var observableFromApi: Observable<List<Cryptoсurrency>>? = null
        if (hasConnection) {
            observableFromApi = getCryptocurrenciesFromApi()
        }

        return observableFromApi!!
    }

    private fun getCryptocurrenciesFromApi(): Observable<List<Cryptoсurrency>> {
        return apiInterface.getCryptoсurrencies(1, 30, "USD")
            .doOnNext {
                Timber.e(it.size.toString())
//                for (item in it) {
//                    //cryptoCurrencyDao.insertCryptocurrency(item)
//                }
            }
    }

//    private fun getCryptocurrenciesFromDb(limit: Int, offset: Int): Observable<List<CryptoCurrency>> {
//        return cryptoCurrencyDao.queryCryptocurrencies(limit, offset)
//            .toObservable()
//            .doOnNext {
//                Timber.e(it.size.toString())
//            }
//    }
}