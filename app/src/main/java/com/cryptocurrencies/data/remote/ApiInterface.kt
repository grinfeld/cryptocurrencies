package com.cryptocurrencies.data.remote


import com.cryptocurrencies.data.model.Cryptoсurrency
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by GrinfeldRa
 */
interface ApiInterface {

    @GET("cryptocurrency/listings/latest")
    fun getCryptoсurrencies(@Query("start") start: Int, @Query("limit") limit: Int, @Query("convert") convert: String): Observable<List<Cryptoсurrency>>

}