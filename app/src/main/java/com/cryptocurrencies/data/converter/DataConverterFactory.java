package com.cryptocurrencies.data.converter;

import androidx.annotation.Nullable;
import com.cryptocurrencies.data.model.Data;
import com.google.gson.reflect.TypeToken;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;


/**
 * Created by GrinfeldRa
 */

public class DataConverterFactory extends Converter.Factory {

    @Nullable
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Type myType = TypeToken.getParameterized(Data.class, type).getType();
        final Converter<ResponseBody, Data> delegate = retrofit.nextResponseBodyConverter(this, myType, annotations);
        return new Converter<ResponseBody, Object>() {
            @Override
            public Object convert(ResponseBody body) throws IOException {
                Data<?> data = delegate.convert(body);
                return data.getResponse();
            }
        };
    }
}
